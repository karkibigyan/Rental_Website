package com.stosh;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class User {
	private String fname;
	private String Lname;
	private String address;
	private String email;
	private String password;
	private int id;
	private Object image;
	private String phno;
	private String gender;
	private Date date;
	List<Upload> uploadedList=new ArrayList();
	
	
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getPhno() {
		return phno;
	}
	public void setPhno(String phno) {
		this.phno = phno;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Object getImage() {
		return image;
	}
	public void setImage(Object image) {
		this.image = image;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return Lname;
	}
	public void setLname(String lname) {
		Lname = lname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Upload> getUploadedList() {
		return uploadedList;
	}
	public void setUploadedList(List<Upload> uploadedList) {
		this.uploadedList = uploadedList;
	}
	
	
	

}
