package com.stosh;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;






public class FileDao {
	public void idInsert(User user){
		Connection con=Connector.getConnetor();
		String qry="insert into data(fname,lname,address,email,password,phone,gender) value(?,?,?,?,?,?,?)";
		try {
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setString(1, user.getFname());
			pst.setString(2, user.getLname());
			pst.setString(3, user.getAddress());
			pst.setString(4, user.getEmail());
			pst.setString(5, user.getPassword());
			pst.setString(6, user.getPhno());

			pst.setString(7, user.getGender());
			pst.execute();
			con.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public User idSelection(String email,String password){
		Connection con=Connector.getConnetor();
		String qry="select * from data where email=? and password=? ";
		try{
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setString(1,email);
			pst.setString(2,password);
			
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
				User validatedUser = new User();
				validatedUser.setId(rs.getInt("id"));
				validatedUser.setFname(rs.getString("Fname"));
				validatedUser.setAddress((rs.getString("Address")));
				validatedUser.setEmail((rs.getString("Email")));
				validatedUser.setGender((rs.getString("gender")));
				validatedUser.setLname((rs.getString("Lname")));
				validatedUser.setPhno((rs.getString("phone")));
				validatedUser.setPassword(rs.getString("Password"));
		
				return validatedUser;
			}
			con.close();
		
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return null;
		}

	public void insertPhoto(Upload upload){
		Connection con=Connector.getConnetor();
		String qry="insert into upload(type,location,bed,bath,description,price,userId) value(?,?,?,?,?,?,?)";
		try {
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setString(1,upload.getType());
			pst.setString(2,upload.getLocation());
			pst.setString(3,upload.getBeds());
			pst.setString(4,upload.getBaths());
			pst.setString(5,upload.getDescription());
//			pst.setBlob(6,((Part)upload.getImage()).getInputStream());
			pst.setString(6, upload.getPrice());
			pst.setInt(7, upload.getUserId());
			
			pst.execute();
			con.close();
			
			
			
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
			}
	public Upload getDetails(int id){
		Connection con=Connector.getConnetor();
		
		try {
			String qry="select *from upload where id=?";
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setInt(1,id);
			Upload upload=new Upload();
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
				upload.setId(rs.getInt("id"));
				upload.setType(rs.getString("type"));
				upload.setLocation(rs.getString("location"));
				upload.setBeds(rs.getString("bed"));
				upload.setBaths(rs.getString("bath"));
//				upload.setBlob(rs.getString("image"));
				upload.setPrice(rs.getString("price"));
				upload.setDescription(rs.getString("description"));
				return upload;
			}
			
			con.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return null;
		
		
	}
	public List<Upload> uploadList(){
		Connection con=Connector.getConnetor();
		String qry="select * from upload";
		try {
			PreparedStatement pst=con.prepareStatement(qry);
			ResultSet rs=pst.executeQuery();
			List<Upload> uploadList=new ArrayList();
			while(rs.next()){
				Upload upload=new Upload();
				upload.setId(rs.getInt("id"));
				upload.setType(rs.getString("type"));
				upload.setLocation(rs.getString("location"));
				upload.setBeds(rs.getString("bed"));
				upload.setBaths(rs.getString("bath"));
//				upload.setBlob(rs.getString("image"));
				upload.setPrice(rs.getString("price"));
				upload.setDescription(rs.getString("description"));
				uploadList.add(upload);
				
			}
			con.close();
			return uploadList;
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	public User updateProfile(User user){
		System.out.println("---->>>>>>>>dao");
		Connection con=Connector.getConnetor();
		String qry="update data set Fname=?,Lname=?,Address=?,Email=?,Password=?,phone=?,gender=? where id=?";
		try {
			System.out.println("---->>>>>>>>");
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setString(1,user.getFname());
			pst.setString(2,user.getLname());
			pst.setString(3,user.getAddress());
			pst.setString(4,user.getEmail());
			pst.setString(5,user.getPassword());
			pst.setString(6,user.getPhno());
			pst.setString(7,user.getGender());
			pst.setInt(8,user.getId());
			pst.executeUpdate();
			con.close();
			 User user1=new User();
			 FileDao fd =new FileDao();
			 user1=fd.idSelection(user.getEmail(), user.getPassword());
			 return user1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public List<Upload> uploadedList(int userId){
		Connection con=Connector.getConnetor();
		String qry="select * from upload where userId=?";
		try {
			PreparedStatement pst=con.prepareStatement(qry);
			pst.setInt(1,userId);
			System.out.println("-->>>uploadedList");
			ResultSet rs=pst.executeQuery();
			List<Upload> uploadedList=new ArrayList();
			while(rs.next()){
				Upload upload=new Upload();
				upload.setId(rs.getInt("id"));
				upload.setType(rs.getString("type"));
				upload.setLocation(rs.getString("location"));
				upload.setBeds(rs.getString("bed"));
				upload.setBaths(rs.getString("bath"));
//				upload.setBlob(rs.getString("image"));
				upload.setPrice(rs.getString("price"));
				upload.setDescription(rs.getString("description"));
				uploadedList.add(upload);
				
			}
			con.close();
			return uploadedList;
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}


}
