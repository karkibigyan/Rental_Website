package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.stosh.FileDao;
import com.stosh.Upload;

/**
 * Servlet implementation class ViewServlet1
 */
@WebServlet("/ViewAllProp")
public class ViewServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try
	{
	FileDao fd=new FileDao();
	
	
	List<Upload> uploadList=fd.uploadList();
		request.setAttribute("uploadList", uploadList);
		request.getRequestDispatcher("page.jsp").forward(request, response);
		System.out.println("--->>>viewServlet");
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
