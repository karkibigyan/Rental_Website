package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.stosh.FileDao;
import com.stosh.Upload;

/**
 * Servlet implementation class ViewServlet2
 */
@WebServlet("/ViewServlet2")
public class ViewServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		try{
		FileDao fd=new FileDao();
		Upload upload=fd.getDetails(Integer.parseInt(request.getParameter("id")));
		
		
		
		if(request.getParameter("readMore")!=null){
				
				System.out.println("--->>ViewServlet2 for readmore");
				request.setAttribute("upload",upload);
			request.getRequestDispatcher("details.jsp").forward(request, response);
		}
			else
				
				if(request.getParameter("editPost")!=null){
			
				System.out.println("--->>ViewServlet for editpost");
				request.setAttribute("upload",upload);
				request.getRequestDispatcher("upload.jsp").forward(request, response);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
