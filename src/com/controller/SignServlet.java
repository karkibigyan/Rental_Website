package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.stosh.FileDao;
import com.stosh.Upload;
import com.stosh.User;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/sign")
public class SignServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Object signup;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		FileDao fileDao=new FileDao();
		try{
	if(request.getParameter("signup")!=null){
		
		
			User user=new User();
			user.setFname(request.getParameter("fname"));
			user.setLname(request.getParameter("lname"));
			user.setEmail(request.getParameter("email"));
			user.setAddress(request.getParameter("address"));
			user.setPassword(request.getParameter("password"));
			user.setGender(request.getParameter("gender"));
			user.setPhno(request.getParameter("phoneno"));
			
			
			fileDao.idInsert(user);
			request.setAttribute("signup", signup);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		
	}else if(request.getParameter("signin")!=null){
		System.out.println("---->>>" + request.getParameter("signin"));
		String email=request.getParameter("email1");
		String password=request.getParameter("password1");
	    System.out.println(email);
		System.out.println(password);
        User validatedUser = fileDao.idSelection(email,password);
		System.out.println(validatedUser);
		if(validatedUser!=null){			
			HttpSession session  =  request.getSession();
			List<Upload> uploadedList=fileDao.uploadedList(validatedUser.getId());
			validatedUser.setUploadedList(uploadedList);
			System.out.println("found");
			session.setAttribute("validatedUser", validatedUser);
			request.getRequestDispatcher("Rental.jsp").forward(request, response);
			
		}
		
		else{
//			
		
			response.sendRedirect("login.jsp");
		}
		
	
}}catch(Exception e){
	e.printStackTrace();
}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
