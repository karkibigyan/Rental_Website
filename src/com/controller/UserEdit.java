package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

import com.stosh.FileDao;
import com.stosh.User;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/UserEdit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileDao fd=new FileDao();
		try
		{
			

			User user=new User();
			System.out.println("--->>receiving");
			user.setFname(request.getParameter("fname"));
			user.setLname(request.getParameter("lname"));
			user.setEmail(request.getParameter("email"));
			user.setAddress(request.getParameter("address"));
			user.setPassword(request.getParameter("password"));
			user.setGender(request.getParameter("gender"));
			System.out.println(user.getGender());
			user.setPhno(request.getParameter("phoneno"));
			user.setId(Integer.parseInt(request.getParameter("id")));
			System.out.println("-coplete");
			User validatedUser=fd.updateProfile(user);
			if(validatedUser!=null){
			HttpSession session  =  request.getSession();
			System.out.println(user.getId());
			session.setAttribute("validatedUser", validatedUser);
			
			request.getRequestDispatcher("profile.jsp").forward(request, response);
			}
			else{
				
				response.sendRedirect("profile.jsp");
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
