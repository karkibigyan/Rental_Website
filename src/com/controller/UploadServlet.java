package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.websocket.Session;

import com.stosh.FileDao;
import com.stosh.Upload;

/**
 * Servlet implementation class upload
 */
@WebServlet(urlPatterns="/upload")
@MultipartConfig(maxFileSize = 16177215)
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileDao fd=new FileDao();
		
		try{
			if(request.getParameter("upload")!=null){
			Upload upload=new Upload();
			Part p=request.getPart("image");
			upload.setImage(p);
			upload.setUserId(Integer.parseInt(request.getParameter("userId")));
			upload.setType(request.getParameter("type"));
			upload.setLocation(request.getParameter("location"));
			upload.setBeds(request.getParameter("beds"));
			upload.setBaths(request.getParameter("baths"));
			upload.setPrice(request.getParameter("price"));
			upload.setDescription(request.getParameter("description"));
			System.out.println(upload.getDescription());
			System.out.println("-------->>");
			fd.insertPhoto(upload);
			System.out.println("<<<<<<--------");
			request.getRequestDispatcher("profile.jsp").forward(request, response);
			
			
			}else if(request.getParameter("readMore")!=null){
				Upload upload=new Upload();
				int id=Integer.parseInt(request.getParameter("id"));
				upload=fd.getDetails(id);
				request.setAttribute("upload", upload);
				request.getRequestDispatcher("details.jsp").forward(request, response);
				
				
				
			}
			

		
		}catch(Exception e){
			e.printStackTrace();
		}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
