<%@page import="com.stosh.*" %>
<!DOCTYPE doctype html>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="#">
                  Home
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
              <% User validatedUser=(User)session.getAttribute("validatedUser");
              if(validatedUser==null){ %>
              <li>
               <a href="login.jsp">SignIn </a>
              </li>
              <%}else {%>
               <li>
               <a href="profile.jsp"><%=validatedUser.getFname() %></a>
              </li>
               <li>
               <a href="upload.jsp">Upload</a>
              </li>
              
            <%} %>
            </ul>
          </div>
        </div>
      </nav>
    </div>
<div class="container center-block">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fa-align-center">
					
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<p class="text-right phone-top">
						
					</p>
				</div>
			</div><hr />
			
			<% Upload upload=(Upload)request.getAttribute("upload"); %>
			
			
			
					<div class="panel panel-default pan-property center-block">
						  <img class="img-responsive" src="../new_slider02-800x400.jpg" alt="">
						<div class="panel-body">
							<h4>
								<a href="single.html" title="">RENTAL NEPAL</a>
							</h4>
						</div>
						<div class="list-group">
							 <span class="list-group-item">Type: <span><%=upload.getType() %></span></span>
                        <span class="list-group-item">City: <span><%=upload.getLocation() %></span></span> 
                        <span class="list-group-item">Location:ABC</span></span>  
							<span class="list-group-item">Bedrooms: <span><%=upload.getBeds() %></span></span> 
							<span class="list-group-item">Bathrooms: <span><%=upload.getBaths() %></span></span>
                            <span class="list-group-item width">Map: <span>Put the map here sudb</span></span>
                            <div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><strong><%=upload.getPrice() %></strong></p>
										</div>
										 <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" name="description" maxlength="140" rows="7"><%=upload.getDescription() %></textarea>
                                            
                    </div>
                    
					</div>
                    </div>
                    </div>
                            <span><div class="container">
<div class="row">
<div class="col-sm-12">
<h3>User Comment </h3>
</div><!-- /col-sm-12 -->
</div><!-- /row -->
<div class="row">
<div class="col-sm-1">
<div class="thumbnail">
<img class="img-responsive user-photo" src="../5.jpg">
</div><!-- /thumbnail -->






</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>myusername</strong> <span class="text-muted">commented 5 days ago</span>
</div>
<div class="panel-body">
Best place to stay for a middle class family
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 --> 
                            </span>
                            
						</div>
								</div>
                                </div>
                                </div>
  </body>
  </html>