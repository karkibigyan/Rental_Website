<!DOCTYPE html>
<%@page import="com.stosh.*"%>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1" name="viewport">
           <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="#">
                  Home
                  
                  
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
              <% User validatedUser=(User)session.getAttribute("validatedUser");
             
              if(validatedUser==null){ %>
             
              
              <li>
               <a href="login.jsp">SignIn </a>
              </li>
              <%}else {%>
               <li>
               <a href="profile.jsp"><%=validatedUser.getFname() %></a>
              </li>
               <li>
               <a href="upload.jsp">Upload</a>
              </li>
              
            <%} %>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <section class="top-section">
		<div class="container">
			<div class="jumbotron jumbotron-custom">
				<div class="row">
					<div class="col-xs-9 col-sm-9 col-md-7 col-lg-7">
						<h1>
							Real estate site
						</h1>
						<p>
							This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.
						</p>
						<p>
							<a class="btn btn-primary btn-lg" href="ViewAllProp" name="view1" value="view1">View 1</a>
						</p>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-5 col-lg-5">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<p>
											Property search
										</p>
									</div>
									<form class="bg-rentportal" role="form">
										<fieldset>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													 <select class="form-control " name="TypeSelect"> <option selected="selected"> Type </option>
                                                     <option value="Sale">House</option>
                                                     <option value="Rent">Flat</option>
                                                     <option value="Room">Room</option>
                                                     </select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													 <select class="form-control " name="BedsSelect"> <option selected="selected"> No.Of Bedrooms </option>
                                                     <option value="1">1</option>
                                                     <option value="2">2</option>
                                                     <option value="3">3</option>
                                                     <option value="4">4</option>
                                                     </select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													 <select class="form-control" name="BathsSelect"> <option selected="selected"> No.Of Bathrooms </option>
                                                     <option value="1">1</option>
                                                     <option value="2">2</option>
                                                     <option value="3">3</option>
                                                     <option value="4">4</option>
                                                     </select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
												<div class="form-group">
													 <select class="form-control" name="CitySelect"> <option selected="selected"> City </option>
                                                     <option value="Ktm">Kathmandu</option>
                                                     <option value="Lalit">Lalitpur</option>
                                                     <option value="Bhakt">Bhaktapur</option>
                                                     </select>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
												<div class="form-group">
													 <input type="Search" class="form-control" id="Search" placeholder="Location">
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													 <select class="form-control" name="PriceFromSelect"> <option selected="selected"> Price from </option>
                                                     <option value="1">NRs 1.5k</option>
                                                     <option value="2">NRs 15k</option>
                                                     <option value="3">NRs 150k</option>
                                                     </select>
                                                   
                                                     </select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													 <select class="form-control" name="PriceToSelect"> <option selected="selected"> Price to </option>
                                                    <option value="1">NRs 2.50k</option>
                                                     <option value="2">NRs 450k</option>
                                                     <option value="3">NRs 1500k</option>
                                                    
                                                     </select>
												</div>
											</div>
                                       
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												 <button type="submit" class="btn btn-md pull-right btn-info">SEARCH</button>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <div class="container-fluid find">
      <div class="table">
        <div class="row col-md-6">
          <h1 class="caption1">
            FIND YOUR DESIRED PLACE TO LIVE IN
          </h1>
          <h3 align="left" class="text3">
            RentalNepal Is The Ultimate Solution To real State In Nepal
          </h3>
        </div>
      </div>
    </div>

    <section>
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div>
						<h3>
							FEATURED ITEMS
						</h3>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<p class="text-right phone-top">
						<a class="btn btn-info" type="button" href="#">VIEW ALL PROPERTIES</a>
					</p>
				</div>
			</div><hr />
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="panel panel-default pan-property">
						  <img class="img-responsive" src="img/new_slider02-800x400.jpg" alt="">
						<div class="panel-body">
							<h4>
								<a href="single.html" title="">RENTAL NEPAL</a>
							</h4>
						</div>
						<div class="list-group">
							<span class="list-group-item">Beds <span class="pull-right">2</span></span> 
							<span class="list-group-item">Baths <span class="pull-right">2</span></span>
							<span class="list-group-item">House Size: <span class="pull-right">3200 sqft</span></span>
							<span class="list-group-item">Lot Size: <span class="pull-right">0.50 aqres</span></span>
						</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><strong>$1.500.000</strong></p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><a class="btn btn-md btn-success btn-block" href="#" title="">Read more</a></p>
										</div>
									</div>
								</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="panel panel-default pan-property">
						  <img class="img-responsive" src="img/maxresdefault.jpg" alt="">
						<div class="panel-body">
							<h4>
								<a href="single.html" title="">RENTAL NEPAL</a>
							</h4>
						</div>
						<div class="list-group">
							<span class="list-group-item">Beds <span class="pull-right">2</span></span> 
							<span class="list-group-item">Baths <span class="pull-right">2</span></span>
							<span class="list-group-item">House Size: <span class="pull-right">3200 sqft</span></span>
							<span class="list-group-item">Lot Size: <span class="pull-right">0.50 aqres</span></span>
						</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><strong>$1.500.000</strong></p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><a class="btn btn-md btn-success btn-block" href="#" title="">Read more</a></p>
										</div>
									</div>
								</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="panel panel-default pan-property">
						 <img class="img-responsive" src="img/house-dharan-sunsari.jpg" alt="">
						<div class="panel-body">
							<h4>
								<a href="single.html" title="">RENTAL NEPAL</a>
							</h4>
						</div>
						<div class="list-group">
							<span class="list-group-item">Beds <span class="pull-right">2</span></span> 
							<span class="list-group-item">Baths <span class="pull-right">2</span></span>
							<span class="list-group-item">House Size: <span class="pull-right">3200 sqft</span></span>
							<span class="list-group-item">Lot Size: <span class="pull-right">0.50 aqres</span></span>
						</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><strong>$1.500.000</strong></p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><a class="btn btn-md btn-success btn-block" href="#" title="">Read more</a></p>
										</div>
									</div>
								</div>
					</div>
				</div>
			</div><hr />
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<h3>
						About us
					</h3>
					<p class="lead">
						Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
					</p>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h4>
								Feature title
							</h4>
							<p>
								Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
							</p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h4>
								Feature title
							</h4>
							<p>
								Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
							</p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h4>
								Feature title
							</h4>
							<p>
								Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
							</p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h4>
								Feature title
							</h4>
							<p>
								Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
							</p>
						</div>
					</div>
				</div>
			</div><hr />
		</div>
	</section>

   <!-- <div class="container box">
       <form action='class="search-form"'>
        <div class="form-group has-feedback">
          <label class="sr-only" for="search">
            eg.Dhapakhel,Lalitpur
          </label>
          <input class="form-control" id="search" name="search" placeholder="search" type="text">
            <a href="#">
              <span class="glyphicon glyphicon-search form-control-feedback">
              </span>
            </a>
          </input>
        </div>
      </form> -->
      <!-- <div class="container">
        <div class="row">
          <form class="form-horizontal" id="BudgetForm" role="form">
            <div class="form-group">
              <div class="col-sm-2">
                <label>
                  No.OF Bedrooms
                </label>
                <select class="form-control">
                  <option>
                    1
                  </option>
                  <option>
                    2
                  </option>
                  <option>
                    3
                  </option>
                  <option>
                    4
                  </option>
                  <option>
                    5
                  </option>
                </select>
              </div>
              <div class="col-sm-2">
                <label>
                  No.OF Bathrooms
                </label>
                <select class="form-control">
                  <option>
                    1
                  </option>
                  <
                  <option>
                    2
                  </option>
                  <option>
                    3
                  </option>
                  <option>
                    4
                  </option>
                  <option>
                    5
                  </option>
                </select>
              </div>
              <div class="col-md-3">
                <label>
                  FirstName
                </label>
                <input class="form-control"/>
              </div>
              <div class="col-sm-3">
                <label>
                  MiddleName
                </label>
                <input class="form-control"/>
              </div>
            </div>
          </form>
        </div>
      </div> 
    </div> -->
    <br>
      <!-- <h2 class="featured">
        FEATURED ITEMS
      </h2> -->
      <div class="container">
        <!-- <div class="row">
          <div class="span12">
            <div class="well">
              <div class="carousel slide" id="myCarousel">
                <ol class="carousel-indicators">
                  <li class="active" data-slide-to="0" data-target="#myCarousel">
                  </li>
                  <li data-slide-to="1" data-target="#myCarousel">
                  </li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <div class="row-fluid">
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../new_slider02-800x400.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../550963466d3fbf2015113638.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../60528302-67ee-42e8-96a1-3ce6eb93ae5b.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="row-fluid">
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../maxresdefault.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../Capella Singapore_Contemporary Manor Living Room_1.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                      <div class="span3">
                        <a class="thumbnail" href="#x">
                          <img alt="Image" src="../View from Infront.jpg" style="max-width:100%;"/>
                        </a>
                      </div>
                    </div>
                  </div>
                  <a class="left carousel-control" data-slide="prev" href="#myCarousel">
                    ‹
                  </a>
                  <a class="right carousel-control" data-slide="next" href="#myCarousel">
                    ›
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <footer class="container-fluid text-center bg-lightgray">
          <div class="copyrights" style="margin-top:25px;">
            <p>
              Kathmandu,Nepal © 2016, All Rights Reserved
            </p>
            <a href="#">
              <i aria-hidden="true" class="fa fa-facebook-official">
              </i>
            </a>
            <a href="#">
              <i aria-hidden="true" class="fa fa-twitter">
              </i>
            </a>
            <a href="#">
              <i aria-hidden="true" class="fa fa-instagram">
              </i>
            </a>
          </div>
        </footer>
      </div>
    </br>
  </body>
</html>
