<%@ page import="com.stosh.*" %>
<!DOCTYPE doctype html>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1" name="viewport">
      <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="Rental.jsp">
                  Home
                  
                  
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
              <% User validatedUser=(User)session.getAttribute("validatedUser");
             
              if(validatedUser==null){ %>
             
              
              <li>
               <a href="login.jsp">SignIn </a>
              </li>
              <%}else {%>
               <li>
               <a href="profile.jsp"><%=validatedUser.getFname() %></a>
              </li>
               <li>
               <a href="upload.jsp">Upload</a>
              </li>
              
            <%} %>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    
<div class="container">
      <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad"> 
          <div class="panel panel-info">                
                  <form class="form-horizontal" action="UserEdit" method="post">
  <div class="form-group">
  <br>
    <label class="control-label col-sm-2" for="Fname">FName:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="fname" value=<%=validatedUser.getFname() %>>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="Lname">LName:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="lname" value=<%=validatedUser.getLname() %>>
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="address">Address:</label>
    <div class="col-sm-6">
      <input type="address" class="form-control" name="address" value=<%=validatedUser.getAddress()%>>
    </div>
  </div>
 
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Email:</label>
    <div class="col-sm-6">
      <input type="email" class="form-control" name="email" value=<%=validatedUser.getEmail() %>>
    </div>
  </div>
   <div class="form-group">
    <label class="control-label col-sm-2" for="password">Password:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="password" value=<%=validatedUser.getPassword() %>>
    </div>
  </div>
 
  <div class="form-group">
    <label class="control-label col-sm-2" for="phn">Contact:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="phoneno" value=<%=validatedUser.getPhno() %>>
     
    </div>
  </div>
  <div class="form-group">
  <label class="control-label col-sm-2" for="gender">Gender:</label>
   <div class="col-sm-6">
  <select class="form-control" name="gender"> <option selected="selected"> Male </option>
 <option value="Female">Female</option>
 
 </select>
  <div class="form-group" align="center">
  <input type="hidden" name="id" value=<%=validatedUser.getId() %>>
                <button type="submit" class="btn btn-md btn-info" name="submit" value="submit">Submit</button>
                  <br></div>
      </form>            
                </div>
                </div>
                </div>
                </div>
                </div>

</body>
</html>