<%@page import="java.io.PrintWriter"%>
<%@page import="com.stosh.*" %>
<%@page import="java.util.*" %>


<%@page import="com.controller.*" %>
<!DOCTYPE doctype html>
<html>
  <head>
  
       <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="Rental.jsp">
                  Home
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
              <% User validatedUser=(User)session.getAttribute("validatedUser");
              
              if(validatedUser==null){ %>
              <li>
               <a href="login.jsp">SignIn </a>
              </li>
              <%}else {%>
               <li>
               <a href="profile.jsp"><%=validatedUser.getFname() %></a>
              </li>
               <li>
               <a href="upload.jsp">Upload</a>
              </li>
              
            <%} %>
            </ul>
          </div>
        </div>
      </nav>
    </div>
<div class="container">
			<div class="row">
			
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div>
						<h3>
							FEATURED ITEMS
							<% List<Upload> uploadList=(ArrayList)request.getAttribute("uploadList"); %>
							
						
						</h3>
					</div>
				</div>
			
			
				
			</div><hr />
			<% 
			  for(Upload upload:uploadList){ 
			  %>
			           <div class="container">
			           <div class="row">
			           
			           <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="panel panel-default pan-property">
						  <img class="img-responsive" src="" alt="">
						<div class="panel-body">
							<h4>
								<a href="Rental.jsp" title="">RENTAL NEPAL</a>
							</h4>
						</div>
						<div class="list-group">
							<span class="list-group-item">Type:<span class="pull-right"><%=upload.getType() %></span></span> 
							<span class="list-group-item">Location:<span class="pull-right"><%=upload.getLocation() %></span></span>
							<span class="list-group-item">Price: <span class="pull-right"><%=upload.getPrice() %></span></span>
						
						</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									
											<p><a class="btn btn-md btn-success btn-block" href="ViewServlet2?id=<%=upload.getId() %>" name="readMore"  value="readMore">Read more</a></p>
										</div>
									</div>
								</div>
					</div>
				</div>
				
			</div><hr />
			
			</div>
			
			</div>
			<% } %>
			
			
			</body>
			</html>
			