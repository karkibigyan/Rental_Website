    <%@ page import="com.stosh.*" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
   <meta content="width=device-width,intial-scale=1" name="viewport">

      <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="Rental.jsp">
                  Home
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
              <% User validatedUser=(User)session.getAttribute("validatedUser"); %>
             
          
               <li>
               <a href="#"><%=validatedUser.getFname() %></a>
              </li>
              <li>
                <a href="upload.jsp">
                  Upload
                </a>
              
            </ul>
          </div>
        </div>
      </nav>
    </div>
<div class="container">
      <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="../5.jpg" class="img-circle img-responsive"> </div>
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Name:</td>
                        <td><%=validatedUser.getFname()%><%= "   "%><%=validatedUser.getLname() %></td>
                      </tr>
                      <tr>
                        <td>Address:</td>
                        <td><%=validatedUser.getAddress() %></td>
                      </tr>                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td><%=validatedUser.getGender() %></td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>Nepal</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com"><%=validatedUser.getEmail() %></a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><%=validatedUser.getPhno() %>
                        </td>
                           
                   
                     
                    </tbody>
                  </table>
                  

                  <a href="profileEdit.jsp" class="btn btn-primary">Edit</a>
                                    <a href="logout.jsp" class="btn btn-primary">Log out</a>
                                    
                                  
                                  
                                 
                </div>
              </div>
            </div>
                </div>
                </div>
                </div>
               
                </div>
                 <div class="container ">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div align="justify">
						<h3>
							MY POSTS
						</h3>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				
				</div>
			</div><hr />
                
                
                
                <% List<Upload> uploadedList= validatedUser.getUploadedList();%>
                <%for(Upload upload:uploadedList){ %>
               
               <div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<div class="panel panel-default pan-property">
						  <img class="img-responsive" src="../new_slider02-800x400.jpg" alt="">
						<div class="panel-body">
							<h4>
                             <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right">
											<a class="btn btn-md btn-success btn-block" href="ViewServlet2?id=<%=upload.getId() %>"  name="editPost"  value="editPost">Edit</a>
										</div>
								<a href="single.html" title="">RENTAL NEPAL</a>
                                
                               
							</h4>
						</div>
						<div class="list-group">
							 <span class="list-group-item">Type:<span class="pull-right"><%=upload.getType() %></span></span> 
                        <span class="list-group-item">Location:<span class="pull-right"><%=upload.getLocation() %></span></span>  
							
						</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><strong><%=upload.getPrice() %></strong></p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<p><a class="btn btn-md btn-success btn-block" href="ViewServlet2?id=<%=upload.getId()%>" name="readMore"  value="readMore">Read more</a></p>
											</div>
											</div>
											</div>
											</div>
											</div>
											</div>
											</div>
											<%}%>
							

</body>
</html>											
