<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1" name="viewport">
         <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="Rental.jsp">
                  Home
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">
                  Contact
                </a>
              </li>
             
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <br><h2 align="center">SIGN IN</h2>
    <form action="sign" class="form-horizontal"  method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email1" name="email1">Email:</label>
    <div class="col-sm-6">
      <input type="email" class="form-control" id="email1" name="email1" placeholder="Enter email" required>
     
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="password1" name="password1">Password:</label>
    <div class="col-sm-6"> 
      <input type="password" class="form-control" id="password1" name="password1" placeholder="Enter password" required>
    </div>
  </div>
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
       <button type="submit" class="btn btn-md btn-info" name="signin" value="signin">SIGN IN</button>
       
    </div>
  </div>
</form>

<br><h2 align="center">SIGN UP</h2>
 <form action="sign" class="form-horizontal" method="get">
  <div class="form-group">
 
    <label class="control-label col-sm-2" for="fname">Fname:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="fname" placeholder="Enter your first name" required>
     
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="lname">Lname:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="lname" placeholder="Enter your last name" required>
    
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="address">Address:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="address" placeholder="Enter your Address" required>
     
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Email:</label>
    <div class="col-sm-6">
      <input type="email" class="form-control" name="email" placeholder="Enter email" required>
     
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="passsword">Password:</label>
    <div class="col-sm-6"> 
      <input type="password" class="form-control" name="password" placeholder="Enter password" required>
    </div>
  </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="phno">Phone No:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="phno" placeholder="Enter phone number" required>
     
    </div>
  </div>
  
  <div class="form-group">
   <label class="control-label col-sm-2" for="gender">Gender:</label>
    <div class="control-label col-sm-2">
 <select class="form-control" name="genderSelect"> <option selected="selected"> Male </option>
 <option value="Female">Female</option>
 
 </select>
												</div>
											</div>
 
      <div class="checkbox">
        <label class="col-sm-offset-2 col-sm-10"><input type="checkbox" value="rememberMe" name="rememberMe"> Remember me</label>
      </div>
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-md btn-info" name="signup" value="signup">SIGN UP</button>
    
              
    </div>
  </div>
</form>

  

</body>
</html>