<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.stosh.*" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1" name="viewport">
           <link href="assets/css/b.css" rel="stylesheet">
        <script src="js/j.js">
        </script>
        <script src="js/b.js">
    
       </script>
        <script src="Untitled-1.js">
        </script>
        <title>
          Rent portal
        </title>
        <link href="assets/css/style.css" rel="stylesheet">
          <link href="assets/css/font.css" rel="stylesheet">
</head>
<body>
<div class="container-fluids">
      <nav class=" back navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="text navbar-brand active" href="#">
              RentalNepal
              <i aria-hidden="true" class="fa fa-home">
              </i>
            </a>
          </div>
          <div class=" text2 back collapse navbar-collapse text3" id="myNavbar">
            <ul class=" pull-right nav navbar-nav">
              <li class="active">
                <a href="Rental.jsp">
                  Home
                </a>
              </li>
              <li>
                <a href="about.jsp">
                  About
                </a>
              </li>
              <li>
                <a href="contact.jsp">Contact</a>
              </li>
               <% User validatedUser=(User)session.getAttribute("validatedUser");
               Upload upload=(Upload)request.getAttribute("upload");
               if(upload==null){
            	  upload=new Upload();
            	   upload.setBaths("");
            	   upload.setBeds("");
            	   upload.setDescription("");
            	   upload.setLocation("");
            	   upload.setPrice("");
            	   upload.setPrice("");
            	   upload.setType("");
            	   upload.setId(0);
            	   
               }
              %>
            
               <li>
               <a href="profile.jsp"><%=validatedUser.getFname() %></a>
              </li>
               <li>
               <a href="#">Upload</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
<div class="container"> <br />
    <div class="row">

    	<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Upload Your Property Image</strong> <small> </small></div>
				<div class="panel-body">
					<div class="input-group image-preview">
						<input placeholder="" type="text" class="form-control image-preview-filename" disabled="disabled">
						<!-- don't give a name === doesn't send on POST/GET --> 
						<span class="input-group-btn"> 
						<!-- image-preview-clear button -->
						<button type="button" class="btn btn-default image-preview-clear" style="display:none;"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
						<!-- image-preview-input -->
						<div class="btn btn-default image-preview-input"> <span class="glyphicon glyphicon-folder-open"></span> <span class="image-preview-input-title">Browse</span>
							<input type="file" accept="image/png, image/jpeg, image/gif" name="image"/>
							<!-- rename it --> 
						</div>
						
						</span> </div>
					<div class="upload-drop-zone" id="drop-zone"> Or drag and drop files here </div>
					<br />
					<!-- Progress Bar -->
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> <span class="sr-only">60% Complete</span> </div>
                        	<br />
					<!-- Upload Finished -->
					<div class="js-upload-finished">
						<h4>Upload history</h4>
						<div class="list-group"> <a href="#" class="list-group-item list-group-item-danger"><span class="badge alert-danger pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue-01.xls</a> <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">23-11-2014</span>amended-catalogue.xls</a> </div>
					</div>
				</div>
                </br>
			
    	
        <div class="container">
<div class="col-md-5">
    <div class="form-area">  
        <form role="form" action="upload" method="post" enctype="multipart/form-data">
        <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Details</h3>
    				<div class="form-group">
						   <label>Type: </label>
            <select id="combobox" name="type">
                <option></option>
                <option value=<%=upload.getType() %> selected><%=upload.getType() %></option>
                <option value="flat">Flat</option>
                <option value="room">Room</option>
                 <option value="flat">House</option>
          </select>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="location" name="location" placeholder="Location(eg.Dhapakhal,Lalitpur)" value=<%=upload.getLocation() %> required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="Price" name="price" placeholder="Price(eg.Rs.1,00,000" value=<%=upload.getPrice() %>required>
					</div>
					<div class="form-group">
                    <label>No. of Bedrooms: </label>
						<select id="combobox2" name="beds">
                <option></option>
                 <option value=<%=upload.getBeds() %>><%=upload.getBeds() %></option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
          </select>
          </div>
          <div class="form-group">
                    <label>No. of Bathrooms: </label>
						<select id="combobox3" name="baths">
                <option></option>
                <option value=<%=upload.getBaths() %> selected><%=upload.getBaths() %></option>
                  <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                 <option value="5">5</option>
          </select>
					</div>    
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" name="description" value=<%=upload.getDescription() %>placeholder="Description" maxlength="140" rows="7">Description</textarea>
                        <span class="help-block"><p id="characterLeft" class="help-block ">Rental Nepal</p></span>                    
                    </div>
            <input type="hidden" name="userId" value=<%=validatedUser.getId() %>>
            <input type="hidden" name="postId" value=<%=upload.getId() %>>
        <%if(upload.getId()==0){ %>
        <button type="submit" id="submit" name="upload" class="btn btn-primary pull-right" value="upload">Upload</button>
        <%}  else{%>
         <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right" value="submit">Submit</button>
         <%} %>
        </form>
    </div>
</div>
</div>
        </div>
		</div>
        

</body>
</html>